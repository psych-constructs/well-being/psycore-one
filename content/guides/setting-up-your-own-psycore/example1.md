---
date: "2019-05-05T00:00:00+01:00"
draft: false
linktitle: Setting up your PsyCoRe
menu:
  example:
    parent: Setting up PsyCoRe
    weight: 1
title: Setting up your own PsyCoRe
toc: true
type: docs
weight: 1
---

An explanation of how to add set up your own instance of PsyCoRe.

