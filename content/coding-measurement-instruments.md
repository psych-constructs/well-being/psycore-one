---
layout: single-overview
description: An overview of the instructions for coding measurement instruments
#header:
#  caption: null
#  caption_url: null
#  image: novelist.jpg
title: Coding Measurement Instruments
url: /coding-measurement-instruments/
instructiontype: instructions to code measurement instruments (as for a systematic review)
instructiontypeCapitalized: Instructions to code measurement instruments (as for a systematic review)
dctField: measure_code
psycoreImage: dct_measurement-coding.svg
---

This overview shows the instructions for coding measurement instruments for all constructs in this repository.
