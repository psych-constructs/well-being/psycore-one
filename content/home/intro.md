---
# An instance of the Blank widget.
# Documentation: https://sourcethemes.com/academic/docs/page-builder/
widget: blank

# Activate this widget? true/false
active: true

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 15

title: Intro
subtitle:

design:
  columns: "1"
  background:
    color: #ffffff
    text_color_light: false
  spacing:
    padding: ["20px", "0", "20px", "0"]
---

In psychological science and its applications (e.g. the development of behavior change interventions), "constructs" play a crucial role in explaining and changing human behavior and psychology.

However, psychological constructs are often not unequivocally defined and are often not accompanied by clear instructions for using them. This resulted in problematic heterogeneity in measurement and widespread use of measurement instruments of questionable of absent validity (see the preprint "[Knowing What We're Talking About: Facilitating Decentralized, Unequivocal Publication of and Reference to Psychological Construct Definitions and Instructions](https://doi.org/jnjp)").

To improve this, one way forward is to develop so-called Decentralized Construct Taxonomy specifications, that specify, for one construct at a time, the following things, each available in a separate overview:

<div class="intro-overview-table">
  <div class="intro-overview-row-container">
    <div class="intro-overview-image">
      <img src="images/dct_definition.svg" width="55px" />
    </div>
    <div class="intro-overview-description">
      A comprehensive and accurate definition: <a href="https://psycore.one/definitions">PsyCoRe.one/definitions</a>
    </div>
  </div>
  <div class="intro-overview-row-container">
    <div class="intro-overview-image">
      <img src="images/dct_measurement.svg" width="55px" />
    </div>
    <div class="intro-overview-description">
      Instructions to develop measurement instruments to conduct empirical research: <a href="https://psycore.one/measurement-instruments">PsyCoRe.one/measurement-instruments</a>
    </div>
  </div>
  <div class="intro-overview-row-container">
    <div class="intro-overview-image">
      <img src="images/dct_measurement-coding.svg" width="55px" />
    </div>
    <div class="intro-overview-description">
      Instructions to code measurement instruments to conduct systematic reviews: <a href="https://psycore.one/coding-measurement-instruments">PsyCoRe.one/coding-measurement-instruments</a>
    </div>
  </div>
  <div class="intro-overview-row-container">
    <div class="intro-overview-image">
      <img src="images/dct_aspect.svg" width="55px" />
    </div>
    <div class="intro-overview-description">
      Instructions to elicit construct content to conduct qualitative research: <a href="https://psycore.one/qualitative-data">PsyCoRe.one/qualitative-data</a>
    </div>
  </div>
  <div class="intro-overview-row-container">
    <div class="intro-overview-image">
      <img src="images/dct_aspect-coding.svg" width="55px" />
    </div>
    <div class="intro-overview-image">
      Instructions to coding qualitative data as informative about construct content: <a href="https://psycore.one/coding-qualitative-data">PsyCoRe.one/coding-qualitative-data</a>
    </div>
  </div>
</div>

## PsyCoRe.one

PsyCoRe.one (**Psy**chological **Co**nstruct **Re**pository) has been developed as a fully Open repository that contains such Decentralized Construct Taxonomy specifications (DCT specifications), and offers a number of convenient overviews that make these specifications accessible to all students, researchers, and practitioners.

In addition to the overviews linked to in the list above, there is also an integrated overview available at <a href="https://psycore.one/overview">PsyCoRe.one/overview</a>, as well as dedicated pages for each construct that are linked to from each overvieew using the construct's Unique Construct Identifier (UCID).

## Contribute

You can develop your own DCT specifications and contribute them to PsyCoRe. There is a guide available in the [Guides](https://psycore.one/guides) section.

## Under the hood

PsyCoRe was designed to be as decentralized as possible: there is no central authoritative repository. In fact, we encourage you to start your own!

Under the hood, this repository consists of a [Hugo](https://gohugo.io) website that uses DCT specifications (which have the [YAML format](https://en.wikipedia.org/wiki/YAML), an Open format designed to store data in a human-readable manner).

This approach makes it relatively easy for anybody to set up their own version of PsyCoRe without any need to set up a server: you can use GitLab Pages, GitHub Pages, or providers such as Netlify.

We explain how to do this in a guide in the [Guides](https://psycore.one/guides) section.

<!-- ## Last update -->

<!-- This repository was last updated at `r format(Sys.time(), '%Y-%m-%d at %H:%M:%S %Z (UTC%z)')`. -->
